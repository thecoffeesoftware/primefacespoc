package com.test;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class LazyCarDataModel extends LazyDataModel<Car> {

	private List<Car> datasource = new ArrayList<>();

	public LazyCarDataModel(CarService carService) {
		loadNewCars(carService);
	}

	@Override
	public Car getRowData(String rowKey) {
		for (Car car : datasource) {
			if (car.getId().equals(rowKey)) {
				return car;
			}
		}

		return null;
	}

	@Override
	public Object getRowKey(Car car) {
		return car.getId();
	}

	@Override
	public List<Car> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		List<Car> data = new ArrayList<Car>();

		// filter
		for (Car car : datasource) {
			boolean match = true;

			if (filters != null) {
				for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
					try {
						String filterProperty = it.next();
						Object filterValue = filters.get(filterProperty);
						BeanInfo info = Introspector.getBeanInfo(Car.class);
						String fieldValue = null;
						for (PropertyDescriptor p : info.getPropertyDescriptors()) {
							if (p.getName().equals(filterProperty)) {
								fieldValue = (String) p.getReadMethod().invoke(car, new Object[0]);
							}
						}
						// String fieldValue = String.valueOf(car.getClass().getField(filterProperty).get(car));
						if (filterValue == null || fieldValue.contains(filterValue.toString())) {
							match = true;
						}
						else {
							match = false;
							break;
						}
					}
					catch (Exception e) {
						match = false;
					}
				}
			}

			if (match) {
				data.add(car);
			}
		}

		// rowCount
		int dataSize = data.size();
		setRowCount(dataSize);

		// paginate
		if (dataSize > pageSize) {
			try {
				return data.subList(first, first + pageSize);
			}
			catch (IndexOutOfBoundsException e) {
				return data.subList(first, first + dataSize % pageSize);
			}
		}
		else {
			return data;
		}
	}

	protected void loadNewCars(CarService carService) {
		datasource.clear();
		datasource = carService.createCars(10);
	}
}
