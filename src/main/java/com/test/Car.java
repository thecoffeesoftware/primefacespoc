package com.test;

import java.util.Map;

public class Car {

	private final int year;
	private final String color;
	private final int price;
	private final boolean soldState;
	private String id;
	private String brand;

	public Car(String id, String brand, int year, String color, int price, boolean soldState) {
		this.id = id;
		this.brand = brand;
		this.year = year;
		this.color = color;
		this.price = price;
		this.soldState = soldState;
	}

	public int getYear() {
		return year;
	}

	public String getColor() {
		return color;
	}

	public int getPrice() {
		return price;
	}

	public boolean isSoldState() {
		return soldState;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

}
