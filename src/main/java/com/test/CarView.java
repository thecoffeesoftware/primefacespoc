package com.test;

import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ToggleEvent;
import org.primefaces.event.data.FilterEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.Visibility;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Map;

@ManagedBean(name = "carView")
@ViewScoped
public class CarView implements Serializable {

	private LazyDataModel<Car> lazyModel;

	private DataTable dataTable = new DataTable();

	@ManagedProperty("#{carService}")
	private CarService service;

	@PostConstruct
	public void init() {
		lazyModel = new LazyCarDataModel(service);
	}

	public void setService(CarService service) {
		this.service = service;
	}

	public void onToggle(ToggleEvent toggleEvent) {
		Column c = ((Column) dataTable.getColumns().get((Integer) toggleEvent.getData()));
		c.setFilterValue(null);
		Map<String, Object> filters = dataTable.getFilters();
		if (filters != null && filters.get(c.getId()) != null) {
			filters.remove(c.getId());
			String script = "document.getElementById(\"" + c.getClientId() + ":filter\").value = \"\";";
			RequestContext.getCurrentInstance().execute(script);
			RequestContext.getCurrentInstance().execute("PF('" + dataTable.getId() + "').filter()");
		}
		c.setVisible(toggleEvent.getVisibility().equals(Visibility.VISIBLE));
	}

	public void onFilter(FilterEvent filterEvent){
	}

	public DataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}

	public LazyDataModel<Car> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Car> lazyModel) {
		this.lazyModel = lazyModel;
	}
}
